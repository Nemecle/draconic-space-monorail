# Draconic space monorail
Procedurally generated absurd public transport network.

Made using [p5.js](http://localhost:8000/p5js.org/)

Try it [here](https://nemecl.eu/projects/draconic-space-monorail).

[Source code](https://framagit.org/Nemecle/draconic-space-monorail)
