function convert_to_base(n, base) {
  
  
  return n;
}

class Alphabet {
  constructor() {
    let alphabets = [
      "abcdefghiklmnopqrstuwxyz",
      "ABCDEFGHIKLMNOPQRSTUWXYZ",
      "123456789",
      "123456789ABCDEF",
      "☉☽︎☿♀♁🜨♂♃♄⛢⧬♅♆⯉⚳⚴⚵⚶⚘♇",
      "ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ",
      "αβγδεζηθικλμνξοπρστυφχψω"
    ];
    
    this.alphabet =  pick(alphabets);
  }

  get_id (index) {
    let base = this.alphabet.length -1;
    //print(`(alphabet) [${this.alphabet}] i: ${index} / ${base} -> ${this.alphabet[(index % base)]}`)
    return this.alphabet[(index % base) ];
  }
}