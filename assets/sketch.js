let pos = 0;
var WIDTH = 500 // -1
var HEIGHT = WIDTH;
var TOTAL_LEGEND_HEIGHT;
var NOISE_SEED;
var RANDOM_SEED;
var NOISE_LOD = 8;
var NOISE_FALLOFF = 0.50;
var NOISE_SCALE;
var GRID_SCALE;

var COAST_VALUE = 0.7;
var POPULATION = [];
var POPULATION_RANKED;
var POPULATION_SIZE;

var name;

var IS_SF    = false;
var IS_MAGIC = false;

var ENV_INTERNAL_STROKE;
var ENV_INTERNAL_COLOUR;

var ENV_EXTERNAL_STROKE;
var ENV_EXTERNAL_COLOUR;

var TRANSPORTS = [];

// schemes = [bg_colour, fg_colour];
var DARK_OLD_SCHOOL_SF_THEME = {
  background: "black",
  foreground: "white"
};
var MODERN_THEME = {
  background: 240,
  foreground: [53, 61, 69]
};
var LIGHT_MAGIC_THEME = {
  background: [240, 232, 204],
  foreground: [89, 85, 72]
};
var DARK_FANTASY_THEME = {
  background: [53, 61, 69],
  foreground: "white"
};

var THEME;

var GENERAL_FONT;
var SHOW_INDIVIDUAL_LINES = false;
var scale_slider;
var DRAW_SCALE = 1;
var x_offset = 0;
var y_offset = 0;

var INTERACTIVE = false;

class Cell {
  constructor(x,y, population=0) {
    this.x = x;
    this.y = y;
    
    this.population = population;
  }
  
  display() {
    strokeWeight(this.population);
    point(this.x * GRID_SCALE, this.y * GRID_SCALE);
  }
}

function get_noise(x,y) {
  return (noise(x * NOISE_SCALE, y * NOISE_SCALE)) * 1.9 - 0.30;
}

function preload() {
  //GENERAL_FONT = loadFont("./inconsolata-v20-latin-ext_latin-regular.ttf");
}

function setup() {

  let seed = find_get_parameter("city");

  if(seed == null || seed == "") {
    seed = random(0,10000);
    name = get_random_city_name(int(random(2,6)));
  } else {
    name = seed;
  }

  let nbr_seed = get_number_from_string(name);
  print(`"${name}" will be used as seed, giving numeric seed ${nbr_seed}`)

  RANDOM_SEED = NOISE_SEED = nbr_seed;
  noiseSeed(NOISE_SEED)
  randomSeed(RANDOM_SEED);
  
  var direct_link = document.getElementById('direct-link');
  direct_link.setAttribute('href', `index.html?city=${name}`);

 
  POPULATION_SIZE = int(random(10000, 50000));
  let number_of_transports = int(random(2,6));
  print(`${number_of_transports} different transports`)
  
  if(SHOW_INDIVIDUAL_LINES) {
    TOTAL_LEGEND_HEIGHT = 55; //8 * GRID_SCALE;
  } else {
    TOTAL_LEGEND_HEIGHT = 25; //5 * GRID_SCALE;
  }
  
  if(INTERACTIVE || WIDTH == -1) {
    //https://www.reddit.com/r/processing/comments/boyl8n/windowwidth_and_windowheight_sometimes_results_in/
    const css = getComputedStyle(canvas.parentElement),
        marginWidth = round(float(css.marginLeft) + float(css.marginRight)),
        marginHeight = round(float(css.marginTop) + float(css.marginBottom)),
        w = windowWidth - marginWidth, h = windowHeight - marginHeight;

    WIDTH  = w // windowWidth;
    HEIGHT = h - (number_of_transports) * TOTAL_LEGEND_HEIGHT;
  }
  //noiseDetail(NOISE_LOD, NOISE_FALLOFF);

  
  NOISE_SCALE = 0.005 //random(0.02, 0.01);
  GRID_SCALE  = WIDTH/100 //int(NOISE_SCALE * 400);
  
  print("seed:", NOISE_SEED, " population size: ", POPULATION_SIZE, " grid scale: ", GRID_SCALE);
  
  for(let x = 0; x < WIDTH/GRID_SCALE; x ++) {
    POPULATION[x] = [];
    for(let y = 0; y < HEIGHT/GRID_SCALE; y ++) {
      POPULATION[x][y] = new Cell(x,y);
    }
  }
  
  let p = 0;
  let it = 0;
  
  while (p < POPULATION_SIZE) {
    
    //let x = int(random(width/GRID_SCALE));
    //let y = int(random(height/GRID_SCALE));
    let x = int(randomGaussian((WIDTH/GRID_SCALE)/2, 50));
    let y = int(randomGaussian((HEIGHT/GRID_SCALE)/2, 50));
    
    // gaussian noise can be quirky
    if (
      x > 0 && x  < WIDTH/GRID_SCALE &&
      y > 0 && y < HEIGHT/GRID_SCALE &&
      get_noise(x * GRID_SCALE,y * GRID_SCALE) > COAST_VALUE &&
      POPULATION[x][y].population < GRID_SCALE * 1.1) {
      
      POPULATION[x][y].population+=1;
      p++;
    }
    else {
      //if as many fails as population, probably a dead run
      it++; 
    }
    
    // safety
    if (it > POPULATION_SIZE) {
      break;
    }
  }
  
  POPULATION_RANKED = get_population_ranked(POPULATION);
  
  if (random() < 0.30) {
    IS_SF = true;
    THEME = pick([DARK_OLD_SCHOOL_SF_THEME])
    print("is SF");
  }

  if (random() < 0.30) {
    IS_MAGIC = true;
    THEME = pick([LIGHT_MAGIC_THEME, DARK_FANTASY_THEME])
    print("is magic");
  }
  
  if (!IS_MAGIC && !IS_SF) {
    THEME = pick([MODERN_THEME])
  }
  
  for(let i = 0; i < number_of_transports; i++) {
    let nlines = random(5,30);
    let rank = abs(random(10,50));
    TRANSPORTS.push(new Transport(POPULATION, POPULATION_RANKED, nlines, rank));
  }
  
  //TRANSPORTS.push(new Transport(POPULATION, POPULATION_RANKED, 10, 0));
  //TRANSPORTS.push(new Transport(POPULATION, POPULATION_RANKED, 30, 50));
  
  let main_canvas = createCanvas(WIDTH, HEIGHT + (TRANSPORTS.length + 1) * TOTAL_LEGEND_HEIGHT);
  main_canvas.parent("canvas");
  
  if(!INTERACTIVE) {
    noLoop();
  }
}

function get_population_ranked(pop) {
  let population_flat = pop.flat();
  
  
  
  population_flat.sort(function(a, b) {
    if (a.population > b.population) {
      return -1;
    }
    if (a.population < b.population) {
      return 1;
    }
    // population must be equal
    return 0;
  });
  
  return population_flat;
}

function get_random_city_name (length = -1) {

  let diphthongs = [
  "ʼd", "ʼy", "aa", "ae", "ah", "ai", "aí", "aî", "ái", "ãi", "am", "âm", "an", "ân", "än", "ån", "aŋ", "ao", "ão", "aq", "au", "äu", "aû", "aw", "ay", "bd", "bf", "bh", "bm", "bp", "bv", "bz", "cg", "çh", "čh", "ci", "cj", "ck", "cö", "cr", "cs", "ct", "cu", "cw", "cx", "cz", "dd", "dg", "dh", "dl", "dł", "dm", "dn", "dp", "dq", "dr", "ds", "dt", "dv", "dx", "dy", "dź", "dż", "ea", "eá", "éa", "ee", "eh", "ei", "eî", "éi", "ej", "em", "ém", "êm", "en", "én", "ên", "eo", "eq", "eu", "eû", "ew", "êw", "ey", "fh", "fx", "gb", "gc", "ge", "gg", "gi", "gj", "gk", "gl", "gm", "gn", "go", "gq", "gr", "gu", "gü", "gv", "gw", "ǥw", "gx", "gy", "gǃ", "gǀ", "gǁ", "gǂ", "hj", "hl", "hm", "hn", "hr", "hs", "hu", "hv", "hx", "hy", "ia", "ie", "ig", "ih", "ii", "il", "im", "ím", "in", "ín", "în", "iŋ", "io", "ío", "iq", "iu", "iú", "iw", "ix", "jj", "jö", "jr", "jx", "kh", "kj", "kk", "kl", "km", "kn", "kp", "kr", "ks", "ku", "kv", "ḵw", "kx", "ky", "ḷḷ", "lr", "lv", "lw", "lx", "md", "mf", "mg", "mh", "ml", "mm", "mn", "mp", "mq", "mt", "mv", "mw", "mx", "nb", "nc", "nd", "nf", "ñg", "ni", "nm", "ńm", "nn", "np", "nq", "nr", "ns", "nt", "nv", "nw", "nx", "nz", "nǃ", "nǀ", "nǁ", "nǂ", "n-", "oa", "oe", "oê", "ôe", "õe", "oh", "oi", "oí", "oî", "ói", "òi", "om", "ôm", "on", "ôn", "ön", "oo", "oq", "or", "ou", "oû", "ow", "ôw", "oy", "oŷ", "øy", "ph", "pl", "pm", "pn", "pp", "pq", "ps", "pt", "pw", "py", "qh", "qk", "qo", "qq", "qv", "qw", "qy", "rh", "rl", "rm", "rn", "rp", "rr", "rs", "rt", "rw", "rz", "sç", "sg", "si", "sj", "sk", "sl", "sp", "sr", "ss", "st", "sv", "sx", "sy", "tf", "tg", "ti", "tj", "tk", "tl", "tł", "tm", "tn", "tp", "tr", "ts", "tt", "tw", "tx", "ty", "tz", "uc", "ue", "ûe", "ug", "uh", "ui", "uí", "úi", "um", "úm", "un", "ún", "ün", "uŋ", "uo", "uq", "ur", "uu", "uw", "uy", "ux", "vg", "vh", "vk", "vn", "vv", "vr", "wr", "wu", "ww", "wx", "xg", "xh", "xi", "xk", "xö", "xs", "xu", "xw", "xx", "xy", "yh", "yi", "yk", "ym", "yn", "yr", "yu", "yw", "yx", "yy", "zi", "zl", "zr", "zs", "zv", "zw", "zz", "ɔn", "œu", "ŋg", "ŋk", "ŋm", "ŋv", "ſh", "ǃʼ", "ǃg", "ǃh", "ǃk", "ǃn", "ǃx"
  ]
  
  let lower_case_name = "";
  if (length == -1) {
    length = random(1,10);
  }
  
  for (let i = 0; i < length; i++) {
    lower_case_name += pick(diphthongs);
  }
  
  let name = lower_case_name.charAt(0).toUpperCase() + lower_case_name.slice(1);
  return name;
}

function draw_relief () {
  for(let x = 0; x < width; x++) {
    for(let y = 0; y < height; y++) {
      let value = get_noise(x, y);
      let pen  = 0;

      if (value > COAST_VALUE) {
        pen = value * 50;
      } else {
        pen = value * 10;
      }

      stroke(pen);
      //min_value = min(get_noise(x,y), min_value);
      //max_value = max(get_noise(x,y), min_value);
      point(x,y);
    }
  }
}

function draw_land () {
  for(let x = 0; x < width; x++) {
    for(let y = 0; y < height; y++) {
      let value = get_noise(x, y);
      let pen  = 0;

      if (value >= COAST_VALUE) {
        pen = value * 50;
        stroke([255,255,255, pen]);
        point(x,y);
      }
    }
  }
}

function draw_ocean () {
  for(let x = 0; x < width; x++) {
    for(let y = 0; y < height; y++) {
      let value = get_noise(x, y);
      let pen  = 0;

      if (value < COAST_VALUE) {
        pen = value * 50;
        stroke([0,0,0, pen]);
        point(x,y);
      }

    }
  }
}

function draw_population() {
  stroke(THEME.foreground);
  for(let x = 0; x < POPULATION.length; x++) {
    for(let y = 0; y < POPULATION[0].length; y++) {
      POPULATION[x][y].display();
    }
  }
}

function draw_city_name(city_name="ASTREOL") {
  
  let text_size = 30;
  let text_x = 10;
  let text_y = text_size + 5;
  
  let padding = 5;
  
  textSize(text_size);
  textStyle(BOLD);
  textFont("Inconsolata");
  
  let text_w = textWidth(city_name);
  let text_h = text_size;
  
  noStroke()
  fill(THEME.foreground);

  
  rect(text_x - padding, text_y - text_h / 1.2 - padding, text_w + padding * 2, text_h + padding * 2);
  
  noStroke()
  fill(THEME.background); //fill(245, 95, 125);

  
  text(city_name, text_x, text_y)
}

function get_network_information () {
  
}

function draw_legend (show_individual_lines=true) {
  

  let top = HEIGHT;
  fill(THEME.background)
  noStroke();
  rect(0, top, WIDTH, HEIGHT);
  
  for (let t = 0; t < TRANSPORTS.length; t++) {
    let transport = TRANSPORTS[t];
    
    let x = 10;
    let y = top + 40+ t * TOTAL_LEGEND_HEIGHT;
    transport.display_legend(x,y, show_individual_lines);
  }
}

function mouseWheel(event) {
  if(INTERACTIVE) {
    DRAW_SCALE -= event.delta/1000;
    DRAW_SCALE = constrain(DRAW_SCALE, 1, 5)

    if(DRAW_SCALE == 1) {
      x_offset = 0;
      y_offset = 0;
    }
  }
}

function mouseDragged() {
  if(INTERACTIVE) {
    if (mouseX > 0 && mouseX < width && mouseY > 0 && mouseY < height) {
      x_offset = mouseX - x_offset;
      y_offset = mouseY - y_offset;
    }
  }
}

function draw() {
  background(THEME.background);
  
  var draw_scale = DRAW_SCALE;
  
  if(INTERACTIVE) {translate(x_offset - draw_scale * x_offset, y_offset - draw_scale * y_offset);}
  if(INTERACTIVE) {scale(draw_scale)}
  //draw_land();
  //draw_relief();
  //draw_ocean();
  draw_population();
  
  for (let transport of TRANSPORTS) {
    transport.display_lines();
  }
  if(INTERACTIVE) {scale(1/draw_scale);}
  if(INTERACTIVE) {translate(-x_offset + draw_scale * x_offset, -y_offset + draw_scale * y_offset);}
  draw_city_name(name);
  draw_legend(SHOW_INDIVIDUAL_LINES);
}
