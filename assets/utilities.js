function find_get_parameter(parameter_name) {
  /* https://stackoverflow.com/questions/5448545/how-to-retrieve-get-parameters-from-javascript*/
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameter_name) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function get_number_from_string(str_seed) {
  /* return a "seed" as a number from a given string */

  let seed = 0;

  for (let i = 0; i < str_seed.length; i++) {
    seed += str_seed.charCodeAt(i);
  }

  return seed;
}

function pick(array_to_pick_from) {
  return array_to_pick_from[int(random(array_to_pick_from.length))];
}

function shuffle_array(array) {
  /* https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array/12646864#12646864 */
  let shuffled = [...array];
  
  for (let i = shuffled.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
  }
  
  return shuffled;
}

function sum_array(array) {
  return array.reduce((a, b) => a + b, 0)
}

function array_includes_array (ar, obj) {
  for (let i = 0; i < ar.length; i++) {
    let is_similar = true;
    for (let ii = 0; ii < ar[i].length; ii++) {
      if (ar[i][ii] != obj[ii]) {
        is_similar = false;
      }
      if (is_similar) {
        return true;
      }
    }
  }
  
  return false;
}


function double_line(begin_x,begin_y, end_x,end_y, external_size=ENV_EXTERNAL_STROKE, external_colour=ENV_EXTERNAL_COLOUR, internal_size=ENV_INTERNAL_STROKE, internal_colour=ENV_INTERNAL_COLOUR) {
  /* begin and end must be 2d vectors */
  
  strokeWeight(external_size);
  stroke(external_colour);
  
  line(begin_x, begin_y, end_x, end_y);
  
  strokeWeight(internal_size);
  stroke(internal_colour);
  
  line(begin_x, begin_y, end_x, end_y);
  
}