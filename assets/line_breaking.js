let DEBUG = false;

function random_split_length (distance, minimum) {
  let total = 0;
  let splitted = [];
  
  let watchdog = 0;
  
  let signed_minimum = Math.sign(distance) * minimum;
  
  while (total != distance) {
      
    let segment = floor(random (signed_minimum, distance));
    
    if(DEBUG) {print("random segment: ", segment)}
    
    total = sum_array(splitted);
    
    if(DEBUG) {print("current total: ", total, splitted);}
    
    if (abs(segment + total) > abs(distance)) {
      if(DEBUG) {print("segment too large! (dist: ", distance," total: ", total," segment: ", segment, ")");}
      segment = distance - total;
      if(DEBUG) {print("setting segment to ", segment);}
    }
    
    if(abs(segment) >= minimum) {
      splitted.push(segment);
      total += segment;
    } else {
      if(DEBUG) {print("segment too short! ", segment, " for min ", minimum);}
    }
    watchdog +=1;
  }
  
  if(DEBUG) {print("splitted end: ", splitted);}
  return splitted;
}

function get_broken_line (p1, p2, minimum=1, diagonals=false) {
  let points = [];
  
  // sign agnostic
  let x_distance = p2.x - p1.x;
  let y_distance = p2.y - p1.y;
  
  let x_minimum = Math.sign(x_distance) * minimum;
  let y_minimum = Math.sign(y_distance) * minimum;
  
  if(DEBUG) {print("from ", p1.x, p1.y, " to ", p2.x, p2.y, "(", x_distance, y_distance, ")");}
  
  if(DEBUG) {print("----------------line X")}
  let x_split = random_split_length(x_distance, minimum); // welcome to my stream
  if(DEBUG) {print("----------------line Y")}
  let y_split = random_split_length(y_distance, minimum);
  
  
  points.push(p1);

  //watchdog = 0; // 1000 loops overall!
  
  let watchdog = 0;
  
  let total_x = 0;
  let total_y = 0;
  
  while((x_split.length != 0 || y_split.length != 0) && watchdog < 1000) {
    let vertical_or_horizontal = random();
    
    
    if (vertical_or_horizontal > 0.5) {
      // https://stackoverflow.com/questions/36069870/how-to-remove-random-item-from-array-and-then-remove-it-from-array-until-array-i
      let x_segment = Number(x_split.splice(floor(random()*x_split.length), 1));
      
      total_x += x_segment;
      if(DEBUG) {print("picked for x: ", x_segment, ". Still ", x_split.length, " elements.")}
      
      let next_point = points[points.length-1].copy();
      
      let offset = createVector(x_segment,0);
      next_point = p5.Vector.add(next_point, offset);
      
      points.push(next_point);
    } else {
      // https://stackoverflow.com/questions/36069870/how-to-remove-random-item-from-array-and-then-remove-it-from-array-until-array-i
      let y_segment = Number(y_split.splice(floor(random()*y_split.length), 1));
      
      total_y += y_segment;
      if(DEBUG) {print("picked for y: ", y_segment, ". Still ", y_split.length, " elements.")}
      
      let next_point = points[points.length-1].copy();
      
      let offset = createVector(0, y_segment);
      next_point = p5.Vector.add(next_point, offset);
      
      points.push(next_point);
    }
    
    watchdog += 1;
  }
  
  if(DEBUG) {print("END ", total_x,total_x)}

  if (watchdog > 1000) {
    if(DEBUG) {print("(get_broken_line) WARNING: watchdog triggered (> 1000 loops)")}
  }
  
  return points;
}
function display_broken_line (broken_line, line_drawing_callback=line) {
  /* requires to set stroke and strokeWeight before calling */
  
  /*for (let s = 0; s < broken_line.length; s++) {
    point(broken_line[s].x * GRID_SCALE,broken_line[s].y * GRID_SCALE);
  }*/
  
  for (let s = 0; s < broken_line.length -1 ; s++) {
    let s1 = broken_line[s];
    let s2 = broken_line[s+1];

    //strokeWeight(5);

    line_drawing_callback(s1.x * GRID_SCALE, s1.y * GRID_SCALE,s2.x * GRID_SCALE, s2.y * GRID_SCALE)
  }
}

function display_double_broken_line(broken_line, begin_x,begin_y, end_x,end_y, external_size=ENV_EXTERNAL_STROKE, external_colour=ENV_EXTERNAL_COLOUR, internal_size=ENV_INTERNAL_STROKE, internal_colour=ENV_INTERNAL_COLOUR) {
  /* begin and end must be 2d vectors */
  
    /* requires to set stroke and strokeWeight before calling */
  
  /*for (let s = 0; s < broken_line.length; s++) {
    point(broken_line[s].x * GRID_SCALE,broken_line[s].y * GRID_SCALE);
  }*/
  
  for (let s = 0; s < broken_line.length -1 ; s++) {
    let s1 = broken_line[s];
    let s2 = broken_line[s+1];

    //strokeWeight(5);
    
    strokeWeight(external_size);
    stroke(external_colour);
    line(s1.x * GRID_SCALE, s1.y * GRID_SCALE,s2.x * GRID_SCALE, s2.y * GRID_SCALE)
  }
  
  for (let s = 0; s < broken_line.length -1 ; s++) {
    let s1 = broken_line[s];
    let s2 = broken_line[s+1];

    //strokeWeight(5);
    
    strokeWeight(internal_size);
    stroke(internal_colour);
    line(s1.x * GRID_SCALE, s1.y * GRID_SCALE,s2.x * GRID_SCALE, s2.y * GRID_SCALE)
  }
}