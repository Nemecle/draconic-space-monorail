var SF_QUALITY = [
               "automated ",
               "cryogenic ",
               "fusion ",
               "sub-",
               "transient ",
               "null ",
               "atomic ",
               "baryonic ",
               "nuclear ",
               "beaming ",
               "atmospheric ",
               "sentient ",
               "cold-fusion ",
               "cyborg ",
               "teleporting ",
               "subterrean ",
               "space ",
               "quantum ",
               "warp ",
               "deep ",
               "manifold ",
               "tractor-beamed ",
               "hyper ",
               "parallel ",
               "time ",
               "hyper-"];

var MAGIC_QUALITY = [
               "conjured ",
               "divine ",
               "dreamy ",
               "elvish ",
               "celestial ",
               "demonic ",
               "undead ",
               "imperium ",
               "ghostly ",
               "planar ",
               "dwarves' ",
               "faeries' ",
               "astral ",
               "low ",
               "necromancian ",
               "draconic ",
               "high ",
               "slimey ",
               "angelic ",
               "enchanted ",
               ]

var QUALITY = [
               "aero-",
               "underground ",
               "slow ",
               "express ",
               "individual ",
               "kinectic ",
               "double-decker ",
               "gravity-powered ",
               "inter-city ",
               "high-speed ",
               "magnetic ",
               "inclined ",
               "light ",
               "suspended ",
               "articulated ",
               "gyro-"];

var TYPE    = [
               "buses",
               "taxis",
               "trains",
               "ferries",
               "cars",
               "boats",
               "cable cars",
               "tramways",
               "monorails",
               "gondolas",
               "lifts",
               "pods"];

class Transport {
  constructor(population, population_ranked, number_of_lines, ratio_offset) {
    this.type = this.get_transport_type();
    this.lines = [];
    this.ratio_offset = ratio_offset;
    
    this.alphabet = new Alphabet();
    
    this.stroke_weight = random(1,GRID_SCALE - 1);
    
    let colour_type = random();
    
    this.cap = pick([ROUND, SQUARE, PROJECT]);
    
    if (random() < 0.1) {
      this.dashing = [GRID_SCALE,GRID_SCALE*2];
    } else {
      this.dashing = -1;
    }
    
    /*if(colour_type < 0.1) {
      //all lines different random colour
      this.colour = -1;
      this.internal_colour = -1;
    } else */if (colour_type < 0.5) {
      // all lines sames double random colours
      this.colour = [random(100,255), random(100,255), random(100,255)];
      this.internal_colour = [random(100,255), random(100,255), random(100,255)];
    } else {
      // all lines same random colour
      this.colour = [random(100,255), random(100,255), random(100,255)];
      this.internal_colour = -1;
    }
    
    for (let l = 0; l < number_of_lines; l++) {
      this.add_line(population, population_ranked);
    }
  }
  
  add_line(population, population_ranked) {
    let i1 = population_ranked[abs(int(randomGaussian(this.ratio_offset, population.length/2)))];
    let i2 = population_ranked[abs(int(randomGaussian(this.ratio_offset, population.length/2)))];
    
    let departure = createVector(i1.x, i1.y);
    let arriving =  createVector(i2.x, i2.y);
    
    let sub = get_broken_line(departure, arriving, 1);
    
    this.lines.push(sub);

    //stroke("blue")
    
  }

  get_transport_type() {
    let qualities = [];

    qualities.push(...QUALITY);

    if (IS_SF) {
      qualities.push(...SF_QUALITY);
      //print("is SF");
    }

    if (IS_MAGIC) {
      qualities.push(...MAGIC_QUALITY);
      //print("is magic");
    }

    let ttype = "";

    let number_of_qualities = int(randomGaussian(2,1));

    for (let q = 0; q < number_of_qualities; q++) {
      let rdi = int(random(qualities.length));
      let quality = qualities.splice(rdi,1);
      ttype += quality;
    }

    ttype += TYPE[int(random(TYPE.length))];

    return ttype;
  }

  display_line(lline) {
    
    strokeCap(this.cap);
    
    if (this.dashing != -1) {
      drawingContext.setLineDash(this.dashing);
    } else {
      drawingContext.setLineDash([]);
    }
    
    if (this.colour != -1) {
      stroke(this.colour)
      if(this.internal_colour != -1) {
        ENV_INTERNAL_COLOUR = this.internal_colour;
        ENV_INTERNAL_STROKE = this.stroke_weight/2;
        
        ENV_EXTERNAL_COLOUR = this.colour;
        ENV_EXTERNAL_STROKE = this.stroke_weight;
      }
    }
    
    strokeWeight(this.stroke_weight);
    strokeCap(ROUND);
    

    if (this.colour == -1) {
      let random_colour = [random(100,255), random(100,255), random(100,255)];
      stroke(random_colour)
    } else {
      stroke(this.colour)
    }

    if (this.internal_colour != -1) {
      //display_broken_line(sub, double_line);
      display_double_broken_line(lline)
    }
    else {
      display_broken_line(lline);
    }
    
    if (this.dashing != -1) {
      drawingContext.setLineDash([]);
    }
  }

  display_lines() {
    for (let l = 0; l < this.lines.length; l++) {
      let sub = this.lines[l];
      this.display_line(sub);
    }
  }
  
  display_legend(x,y) {
    fill(THEME.foreground);
    noStroke();    
    //rect(x-5,y - 20, 400, 50)
    
    textSize(18); //4 * GRID_SCALE);
    textFont('Inconsolata');
    noStroke();
    
    let text_colour;
    if (this.internal_colour != -1) {
      text_colour = this.internal_colour;
    } else {
      text_colour = this.colour;
    }
    //textStyle(BOLD);
    
    //let bbox = font.textBounds(textString, 10, 30, 12);
    
    text(this.type, x + 100, y)
    
    if(SHOW_INDIVIDUAL_LINES) {
      for (let l = 0; l < this.lines.length; l++) {
        fill(THEME.foreground);
        circle(x + 25 * l + 8, y + 18, GRID_SCALE * 2)
        fill(text_colour);
        text(this.alphabet.get_id(l), x + 25 * l, y + 25);
      }
    }
    
    let legend_line = [];
    
    //legend_line.push(createVector(x,        y));
    //legend_line.push(createVector((x + 90), y));
    
    let x_grid =       x / GRID_SCALE;
    let y_grid =       (y / GRID_SCALE) - 0.6;
    let offset_grid = 90 / GRID_SCALE;
    
    legend_line.push(createVector( x_grid,        y_grid));
    legend_line.push(createVector((x_grid + offset_grid),  y_grid));
    
    this.display_line(legend_line);
  }
}
